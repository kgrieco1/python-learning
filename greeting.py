def greeting(name):
    print("Hello, %s!" % name.title())
    print("How are you, %s?" % name.title())
    print("Have a great day, %s!\n" % name.title())

names = ['jim', 'pam', 'dwight']

print("\n")
for name in names:
    greeting(name)
