cash = [32, 500, 20, 2, 0]
most = max(cash)
least = min(cash)
total = sum(cash)
print("The fattest wallet has $" + str(most) + " in it.")
print("The skinniest wallet has $" + str(least) + " in it.")
print("All together, these wallets have $" + str(total) + " in them.")
