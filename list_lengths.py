days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
schools = ['yorktown', 'washington-liberty', 'wakefield']

print("The number of days in a week is " + str(len(days)) + ".\nThe number of main high schools in Arlington County Public schools is " + str(len(schools)) + ".")
