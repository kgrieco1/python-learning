numbers = [2, 19, 32, 8, 43]

print("You should see the numbers in the order of: 2, 19, 32, 8, 43.")
for number in numbers:
    print(number)
print("\nNow you should see the numbers in increasing order.")
for number in sorted(numbers):
    print(number)
print("\nBack to the original:")
for number in numbers:
    print(number)
print("\nNow the numbers are in decreasing order.")
for number in sorted(numbers, reverse=True):
    print(number)
print("\nBack to the original:")
for number in numbers:
    print(number)
print("\nUp next, the numbers are in reverse order from the original.")
numbers.reverse()
for number in numbers:
    print(number)
print("\nBack to the original:")
numbers.reverse()
for number in numbers:
    print(number)
print("\nNow the numbers will be permanently sorted in increasing order.")
numbers.sort()
for number in numbers:
    print(number)
print("\nAnd now the numbers will be permanently sorted in decreasing order.")
numbers.sort(reverse=True)
for number in numbers:
    print(number)
