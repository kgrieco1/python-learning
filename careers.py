careers = ['programmer', 'truck driver', 'singer', 'journalist', 'nurse', 'doctor', 'teacher', 'repairman', 'actor']

for career in careers:
    print(career.title())
print("\n")
for career in sorted(careers):
    print(career.title())
print("\n")
for career in careers:
    print(career.title())
print("\n")
for career in sorted(careers, reverse=True):
    print(career.title())
print("\n")
for career in careers:
    print(career.title())
careers.reverse()
print("\n")
for career in careers:
    print(career.title())
careers.reverse()
print("\n")
for career in careers:
    print(career.title())
careers.sort()
print("\n")
for career in careers:
    print(career.title())
careers.sort(reverse=True)
print("\n")
for career in careers:
    print(career.title())
