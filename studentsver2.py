def show_students(message, students):
    print(message)
    for student in students:
        print(student.title())

students = ['bernice', 'aaron', 'cody']

students.sort()
show_students("Our students are currently in alphabetical order.", students)
students.sort(reverse=True)
show_students("\nOur students are now in reverse alphabetical order.", students)
