words = []
definitions = []
words.append('list')
definitions.append('a series of strings')
words.append('string')
definitions.append('a sequence of characters assigned to a single variable')
for word, definition in words, definitions:
    sentence = "%s: %s." % (word, definition))
