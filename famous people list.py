celebs = ['kim kardashian', 'kanye west', 'taylor swift', 'katy perry']

print(celebs)
last_celeb = celebs.pop()
print(celebs)
byekanye = celebs.pop(1)
print(celebs)
del celebs[0]
print(celebs)
celebs.remove('taylor swift')
print("Now there are no celebrities. Proof:")
print(celebs)
