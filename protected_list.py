names = ['andrew', 'kelly', 'justin']
copy = names[:]
copy.append('matt')
copy.append('joe')

for name in names:
    print(name.title())
print("This is the original list.\n")
for name in copy:
    print(name.title())
print("This is the copied list.")
