def crowd_test(group):
    print(group)
    if len(group) > 5:
        print("There's a mob in the room right now.")
    elif len(group) >= 3:
        print("The room is kind of crowded right now.")
    elif len(group) >= 1:
        print("The room is not crowded right now.")
    else:
        print("Wow, the room is empty right now.")
    print("\n\n\n")

names = ['jim', 'michael', 'dwight', 'creed', 'stanley', 'ryan']
names2 = names[:-1]
names3 = names2[:-2]
names4 = names3[:-1]
names5 = []

crowd_test(names)
crowd_test(names2)
crowd_test(names3)
crowd_test(names4)
crowd_test(names5)
