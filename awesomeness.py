names = ['jim', 'pam', 'kelly', 'michael', 'dwight']
awesomes = [name.title() + " is awesome!" for name in names]
for awesome in awesomes:
    print(awesome)
