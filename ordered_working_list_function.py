def reverseorder():
    careers.reverse()
    for career in careers:
        print(career.title())
    print("\n")

def original():
    for career in careers:
        print(career.title())
    print("\n")

def alphabetical():
    for career in sorted(careers):
        print(career.title())
    print("\n")

def revalphabetical():
    for career in sorted(careers, reverse = True):
        print(career.title())
    print("\n")

def permalpha():
    careers.sort()
    for career in careers:
        print(career.title())
    print("\n")

def permrevalpha():
    careers.sort(reverse=True)
    for career in careers:
        print(career.title())
    print("\n")

careers = ['programmer', 'truck driver', 'teacher', 'actor']

original()
alphabetical()
original()
revalphabetical()
original()
reverseorder()
reverseorder()
permalpha()
permrevalpha()


