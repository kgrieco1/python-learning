sentence = "I am learning Python because I need to learn a language like Python for the last few days of the internship."
print("Python" in sentence)
print(sentence.find('Python'))
print(sentence.rfind('Python'))
print(sentence.count('Python'))
words = sentence.split(' ')
print(words)
for word in words:
    print(word)
sentence = sentence.replace('Python', 'Ruby')
print(sentence)
