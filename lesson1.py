first_name = "   kevin"
last_name = "grieco        "
full_name = first_name.strip() + ' ' + last_name.strip()
print("Hello, " + full_name.title() + "!")
#The above just stripped whitespace from my name, and combined the first and last to make one full name.

full = full_name.title()
print("Ok, " + full + ". Let's do some math.")
divide = 4/2
print("Dividing 4/2 should get you " + str(divide) + " in Python 3.")
add = 2+2
print("Adding 2+2 should get you " + str(add) + " in Python 3.")
subtract = 5-2
print("Subtracting 2 from 5 should get you " + str(subtract) + " in Python 3.")
multiply = 3*2
print("Multiplying 3*2 should get you " + str(multiply) + " in Python 3.")
exponent = 4**3
print("Raising 4 to the 3rd power should get you " + str(exponent) + " in Python 3.")
combine = 4**(3/2)
print("The order of operation changed the way 4**3/2 was received; since it was written as 4**(3/2), it comes out as " + str(combine) + ".")
#This is the end of the math section.

print("Now I will space out your first and last name with tabs.")
first = first_name.strip()
last = last_name.strip()
print(first.title() + "\t" + last.title())
print("Now I will separate your first and last names with new lines.")
print(first.title() + "\n" + last.title())
print("I did this by typing \\t for tab and \\n for a new line.")
#This is the end of that section.

#This is the end of the code.
